'use strict';

const db = require('../db');

async function create(lesson) {
    let {teacherIds, title, days, firstDate, lessonsCount, lastDate} = lesson;
    if(lessonsCount && lastDate) {
        throw new Error('lessonsCount and lastDate can not be used together');
    }
    let createdLessonsIds = [];
    firstDate = new Date(firstDate);
    if(lastDate) {
        lastDate = new Date(lastDate);
        let lessonDateMoment = firstDate.getTime();
        let lastDateMoment = lastDate.getTime();
        let counter = 1;
        while((lessonDateMoment < lastDateMoment) && (counter <= 300) && ((lessonDateMoment - firstDate.getTime()) < 365 * 24 * 3600000)){
            let lessonDate = new Date();
            lessonDate.setTime(lessonDateMoment);
            lessonDateMoment += 24 * 3600000;
            if(!days.includes(lessonDate.getDay())){
                continue;
            }
            let lesson = await db.Lesson.create({title, date: lessonDate});
            counter++;
            let lessonTeachers = teacherIds.map(id => ({lesson_id: lesson.id, teacher_id: id}));
            await db.LessonTeachers.bulkCreate(lessonTeachers, { fields: ['lesson_id' , 'teacher_id'] });
            createdLessonsIds.push(lesson.id);
        }
       return createdLessonsIds;
    }
    if(Number.isInteger(lessonsCount)){
        lessonsCount = Math.min(lessonsCount, 300);
        let lessonDateMoment = firstDate.getTime();
        for(let counter = 0; (counter < lessonsCount) && ((lessonDateMoment - firstDate.getTime()) < 365 * 24 * 3600000);) {
            let lessonDate = new Date();
            lessonDate.setTime(lessonDateMoment);
            lessonDateMoment += 24 * 3600000;
            if(!days.includes(lessonDate.getDay())){
                continue;
            }
            let lesson = await db.Lesson.create({title, date: lessonDate});
            counter++;
            let lessonTeachers = teacherIds.map(id => ({lesson_id: lesson.id, teacher_id: id}));
            await db.LessonTeachers.bulkCreate(lessonTeachers, { fields: ['lesson_id' , 'teacher_id'] });
            createdLessonsIds.push(lesson.id);
        }
        return createdLessonsIds;
    }
    throw new Error('lessonsCount or lastDate must be used');
}

async function get(filter) {
    let {date, status, teachersIds, studentsCount, page = 1, lessonsPerPage = 5} = filter;
    if(!date) {
        throw new Error('date must to exist');
    }
    let dates = date.split(',');
    let whereLesson = {};
     if(dates.length === 2){
        whereLesson.date = {[db.Sequelize.Op.between]: dates}
    } else {
        whereLesson.date = date;
    }
    if(status) {
        whereLesson.status = status;
    }
    let result = await db.Lesson.findAll({
        where: whereLesson,
        include: [{
            model: db.Teacher
        }, {
            model: db.Student
        }]
    });
    result = JSON.parse(JSON.stringify(result));
    if(studentsCount) {
        let studentsCounts = studentsCount.split(',');
        if(studentsCounts.length === 2) {
            result = result.filter(e => e.students.length >= +studentsCounts[0] && e.students.length <= +studentsCounts[1]);
        } else {
            result = result.filter(e => e.students.length === +studentsCounts[0]);
        }
    }
    if(teachersIds) {
        teachersIds = teachersIds.split(',').map(e => +e);
        result = result.filter(e => e.teachers.some(e => teachersIds.includes(e.id)));
    }
    let offset = (page - 1) * lessonsPerPage;
    result = result.slice(offset, offset + lessonsPerPage);
    for(let lesson of result) {
        let visitCount = 0;
        for(let student of lesson.students) {
            student.visit = student.lesson_students.visit;
            if(student.visit) {
                visitCount++;
            }
            delete student.lesson_students;
        }
        lesson.visitCount = visitCount;
        for(let teacher of lesson.teachers){
            delete teacher.lesson_teachers;
        }
    }
    return result;
}

module.exports = {get, create};