'use strict';

const router = require('koa-router')();

const lessonsController = require('../controllers/lessons');

router.post('/lessons', async (ctx, next) => {
    log('router post', ctx.request.body);
    try {
       ctx.body = await lessonsController.create(ctx.request.body);
    } catch(err) {
       ctx.throw(400, err);
    }
});

router.get('/', async (ctx, next) => {
    log('router get', ctx.query);
    try {
        ctx.body = await lessonsController.get(ctx.query);
    } catch(err) {
        ctx.throw(400, err);
    }
});

module.exports = router;