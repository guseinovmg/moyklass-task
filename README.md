# README #

### Установка

Перед работой надо создать в БД в Postgres, имя пользователя, пароль и имя БД установить в файле config/config.json.
Описание задачи в https://bitbucket.org/guseinovmg/moyklass-task/src/master/backend%20task.pdf

`git clone git@bitbucket.org:guseinovmg/moyklass-task.git`

`cd moyklass-task`

`npm install`

`sudo npm install nodemon mocha -g`  (для запуска тестов)


### Запуск

`nodemon index.js`

### Тест

`mocha test.js`


