const app = require('./index');
const server = app.listen();
const request = require('supertest').agent(server);
const expect = require('expect');

describe('Test', function () {
    after(function () {
        server.close();
    });

    describe('POST /lessons', function () {
        describe('valid input', function () {
            it('create by lesson count', function (done) {
                request
                    .post('/lessons')
                    .send({
                        "teacherIds": [1, 2],
                        "title": "Blue Ocean",
                        "days": [0, 1],
                        "firstDate": "2019-04-10",
                        "lessonsCount": 500
                    })
                    .expect(200, done);
            });
            it('create by last date', function (done) {
                request
                    .post('/lessons')
                    .send({
                        "teacherIds": [1, 2],
                        "title": "Blue Ocean",
                        "days": [0, 1],
                        "firstDate": "2019-04-10",
                        "lastDate": "2019-07-10"
                    })
                    .expect(200, done);
            });
        });

        describe('invalid input', function () {
            it('should 400', function (done) {
                request
                    .post('/lessons')
                    .send({
                        "teacherIds": [1, 2],
                        "title": "Blue Ocean",
                        "days": [0, 1],
                        "firstDate": "2019-04-10"
                    })
                    .expect(400, done);
            });
        });
    });

    describe('GET /', function () {
        describe('valid input', function () {
            it('should work', function (done) {
                request
                    .get('?status=0&page=1&lessonsPerPage=8&date=2019-01-02,2019-12-02&studentsCount=1&teachersIds=1')
                    .expect(200, done);
            });

            it('One date only', async function () {
                await request
                    .get('?date=2019-01-02')
                    .expect(200);
            });

            it('Checking limit', async function () {
                const response = await request
                    .get('?status=0&page=1&lessonsPerPage=2&date=2019-01-02,2020-12-02&studentsCount=0,5&teachersIds=1')
                    .expect(200);
                expect(response.body.length).toEqual(2);
            });

            it('Checking fields', async function () {
                const response = await request
                    .get('?status=0&page=1&lessonsPerPage=2&date=2019-01-02,2020-12-02&studentsCount=2&teachersIds=1')
                    .expect(200);
                const item = response.body[0];
                expect(item).toHaveProperty('visitCount')
            });
        });

        describe('invalid input', function () {
            it('should 400', function (done) {
                request
                    .get('?status=0')
                    .expect(400, done);
            });
        });
    });
});