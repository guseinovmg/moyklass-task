'use strict';

const Sequelize = require('sequelize');
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/config/config.json')[env];
const DataTypes = Sequelize.DataTypes;

const sequelize = new Sequelize(config.database, config.username, config.password, config);

const Model = Sequelize.Model;

class Lesson extends Model {
}

class Teacher extends Model {
}

class Student extends Model {
}

class LessonStudents extends Model {
}

class LessonTeachers extends Model {
}

Lesson.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    date: {
        type: DataTypes.DATE,
        allowNull: false
    },
    title: {
        type: DataTypes.STRING(100)
    },
    status: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    }
}, {
    underscored: true,
    sequelize,
    modelName: 'lesson',
    timestamps: false
});

Teacher.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING(100),
        allowNull: false
    }
}, {
    underscored: true,
    sequelize,
    modelName: 'teacher',
    timestamps: false
});

Student.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING(100),
        allowNull: false
    }
}, {
    underscored: true,
    sequelize,
    modelName: 'student',
    timestamps: false
});


LessonStudents.init({
    lesson_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: 'lessons',
            key: 'id'
        }
    },
    student_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: 'students',
            key: 'id'
        }
    },
    visit: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: false
    }
}, {
    underscored: true,
    sequelize,
    modelName: 'lesson_students',
    timestamps: false
});

LessonTeachers.init({
    lesson_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: Lesson,
            key: 'id'
        }
    },
    teacher_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: Teacher,
            key: 'id'
        }
    }
}, {
    underscored: true,
    sequelize,
    modelName: 'lesson_teachers',
    timestamps: false
});

Lesson.belongsToMany(Teacher, {through: LessonTeachers, foreignKey: 'lesson_id'});
Teacher.belongsToMany(Lesson, {through: LessonTeachers, foreignKey: 'teacher_id'});
Lesson.belongsToMany(Student, {through: LessonStudents, foreignKey: 'lesson_id'});
Student.belongsToMany(Lesson, {through: LessonStudents, foreignKey: 'student_id'});

module.exports = {Sequelize, sequelize, LessonTeachers, Teacher, Student, Lesson, LessonStudents};
