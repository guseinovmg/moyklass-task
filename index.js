"use strict";

const Koa = require('koa');
const koaBody = require('koa-body');
global.log = console.log;
const app = module.exports = new Koa();

app.use(koaBody({
    jsonLimit: '10kb'
}));

app.use(require('./routes/lessons.js').routes());

if (!module.parent) app.listen(3000);